.section .init
.globl _start
_start:

/*
* Branch to Main
*/
b main

/*
* Add to rest of code
*/
.section .text

main:
	
	/*
	* System Setup stack
	*/
	bl SYS_SetStackPosition

	/*
	* Set GPIO function
	*/
	/*
	* Use our new SetGpioFunction function to set the function of GPIO port 16 (OK 
	* LED) to 001 (binary)
	*/
	pinNum .req r0
	pinFunc .req r1
	mov pinNum,#47
	mov pinFunc,#1
	bl GPIO_SetGpioFunction
	.unreq pinNum
	.unreq pinFunc

	/*
	* Set the 15th bit of r1.
	* r1=0x00008000 a number with bit 15 high, so we can communicate with GPIO 47 (32+15=47).
	*/
	mov r1,#1
	lsl r1,#15

	bl GPIO_GetGpioAddress

	/*
	* Load Pattern
	* Note - =pattern loads the address, not the value
	* so [ptrn] dereferences it
	*/
	ptrn .req r4
	ldr ptrn,=pattern
	ldr ptrn,[ptrn]
	seq .req r5
	mov seq,#0

	/*
	* Label the next line loop$ for the infinite looping
	*/
	loop$: 
		mov r0,#47
		mov r1,#1
		lsl r1,seq
		and r1,ptrn
		bl GPIO_SetGpio

		/*
		* Wait
		*/
		ldr r0,=250000
		bl TIMER_Wait

		/*
		* Loop over this process forevermore
		*/
		add seq,#1
		and seq,#0b11111
		b loop$


/*
* Begin Data
*/
.section .data
	.align 2
	pattern:
	.int 0b11111111101010100010001000101010
