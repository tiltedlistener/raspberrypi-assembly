/*
* Map r0 to the beginning of B+ GPIO memory region
*/
.globl GPIO_GetGpioAddress
GPIO_GetGpioAddress:
	ldr r0,=0x20200000
	mov pc,lr

/*
* Set a GPIO Function value
*/
.globl GPIO_SetGpioFunction
GPIO_SetGpioFunction:
	/*
	* Test if our two inputs are valid. 
	* r0 represents the possible pins
	* r1 represents the pin function
	*
	* Note - these are converted to the correct mapping eventually.
	* They are readable to us
	*/
	cmp r0,#53
	cmpls r1,#7
	movhi pc,lr

	/*
	* Move PinNum to r2 so we can use the value of r0 from the return address
	*/
	push {lr}
	mov r2,r0
	bl GPIO_GetGpioAddress

	/*
	* We're using the value of pinNum to see how many increments of 4bytes we need to get to the 
	* right function select spot. 
	*/
	functionLoop$:
		cmp r2,#9
		subhi r2,#10
		addhi r0,#4
		bhi functionLoop$

	/*
	* r2 holds the function location which is a 32-bit encoding for each pin of that group.
	* However, it needs to be moved up x3 since each pin gets 3-bits to encode its function
	*/
	add r2, r2,lsl #1
	lsl r1,r2
	str r1,[r0]
	
	/*
	* Now Return
	*/
	pop {pc}

/*
* Set a value to a GPIO pin
*/
.globl GPIO_SetGpio
GPIO_SetGpio:
	
	/*
	* Localize our variable name
	*/
    pinNum .req r0
    pinVal .req r1

    /*
    * Bounce us if we have chosen a pin more than 53 (the 54th GPIO)
    */
	cmp pinNum,#53
	movhi pc,lr

	/*
	* So we can go back
	*/
	push {lr}

	/*
	* Move pinNum to r2 and reset the variable name so we can put r0 to the GPIO memory space address
	*/
	mov r2,pinNum	
    .unreq pinNum	
    pinNum .req r2
	bl GPIO_GetGpioAddress
    gpioAddr .req r0

    /*
    * Need to store the high/low value in the correct locations r3 holds this
    */
	pinBank .req r3

	/*
	* First take the Pin number and divide by 32
	* 47 / 32 = 1
	*/
	lsr pinBank,pinNum,#5

	/*
	* Now multiple by 4. 4 x 1 = 4
	*/
	lsl pinBank,#2

	/*
	* Set the address map to this offset location
	* We are now bumped up 4 btypes. In other words, controlling all the pins in the second set of 4 bytes	
	*/
	add gpioAddr,pinBank
	.unreq pinBank

	/*
	* Because we are only in 32-bits, we and to get the remainder = 15 for GPIO 47
	*/
	and pinNum,#31

	/*
	* Create setBit to hold our offset location. Use the altered pinNum to create the correct position
	*/
	setBit .req r3
	mov setBit,#1
	lsl setBit,pinNum
	.unreq pinNum

	/*
	* teq - test equals - then drop pinVal. 
	* Store if equal and then store if not equal
	*/
	teq pinVal,#0
	.unreq pinVal
	streq setBit,[gpioAddr,#28]
	strne setBit,[gpioAddr,#40]

	/*
	* Clean up
	*/
	.unreq setBit
	.unreq gpioAddr

	/*
	* Return
	*/
	pop {pc}
