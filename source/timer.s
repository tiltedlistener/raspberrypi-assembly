.globl TIMER_GetSystemTimerBase
TIMER_GetSystemTimerBase: 
	ldr r0,=0x20003000
	mov pc,lr


.globl TIMER_GetTimeStamp
TIMER_GetTimeStamp:
	push {lr}
	bl TIMER_GetSystemTimerBase
	ldrd r0,r1,[r0,#4]
	pop {pc}

.globl TIMER_Wait
TIMER_Wait:
	/*
	* Set delay
	*/
	delay .req r2
	mov delay,r0

	/*
	* For Return
	*/
	push {lr}

	/*
	* Setup start
	*/
	bl TIMER_GetTimeStamp
	start .req r3
	mov start,r0

	/*
	* Check elapsed time
	*/
	loop$:
		bl TIMER_GetTimeStamp
		elapsed .req r1
		sub elapsed,r0,start
		cmp elapsed,delay
		.unreq elapsed
		bls loop$

	/*
	* Clean up
	*/
	.unreq delay
	.unreq start

	/*
	* Return
	*/
	pop {pc}	
