.section .init
.globl _start
_start:

/*
* Branch to Main
*/
b main

/*
* Add to rest of code
*/
.section .text

main:
	
	/*
	* System Setup stack
	*/
	bl SYS_SetStackPosition

	/*
	* Set GPIO function
	*/
	/*
	* Use our new SetGpioFunction function to set the function of GPIO port 16 (OK 
	* LED) to 001 (binary)
	*/
	pinNum .req r0
	pinFunc .req r1
	mov pinNum,#47
	mov pinFunc,#1
	bl GPIO_SetGpioFunction
	.unreq pinNum
	.unreq pinFunc

	/*
	* Set the 15th bit of r1.
	* r1=0x00008000 a number with bit 15 high, so we can communicate with GPIO 47 (32+15=47).
	*/
	mov r1,#1
	lsl r1,#15

	bl GPIO_GetGpioAddress

	/*
	* Label the next line loop$ for the infinite looping
	*/
	loop$: 
		pinNum .req r0
		pinVal .req r1
		mov pinNum,#47
		mov pinVal,#1
		bl GPIO_SetGpio
		.unreq pinNum
		.unreq pinVal

		/*
		* Wait
		*/
		ldr r0,=250000
		bl TIMER_Wait

		/* 
		* Use our new SetGpio function to set GPIO 16 to high, causing the LED to turn 
		* on.
		*/
		pinNum .req r0
		pinVal .req r1
		mov pinNum,#47
		mov pinVal,#0
		bl GPIO_SetGpio
		.unreq pinNum
		.unreq pinVal

		/*
		* Wait once more.
		*/
		ldr r0,=250000
		bl TIMER_Wait

		/*
		* Loop over this process forevermore
		*/
		b loop$








