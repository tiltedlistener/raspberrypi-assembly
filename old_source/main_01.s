.section .init
.globl _start
_start:

/*
* This command loads the physical address of the GPIO region into r0.
*/
ldr r0,=0x20200000

/*
* Our register use is as follows:
* r0=0x20200000 the address of the GPIO region.
* r1=0x00200000 a number with bits 21-23 set to 001 to put into the GPIO
*                               function select to enable output to GPIO 47.
* then
* r1=0x00008000 a number with bit 15 high, so we can communicate with GPIO 47 (32+15=47).
*/
mov r1,#1
lsl r1,#21

/*
* Set the GPIO function select.
*/
str r1,[r0,#16]

/*
* Set the 15th bit of r1.
*/
mov r1,#1
lsl r1,#15

/*
* Set GPIO 47 to high, causing the LED to turn on.
*/
str r1,[r0,#32]

/*
* Loop over this forevermore
*/
loop$:
b loop$