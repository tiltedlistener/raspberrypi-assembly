#!/usr/bin/env bash

# Build the kernel
make

if [ $? -eq 0 ]
then
	 # place in our drive and eject. Pay attention to your Volume name
	cp kernel.img /Volumes/boot/kernel.img
	diskutil unmountDisk force /Volumes/boot
	exit 0 
else
	exit 1
fi


